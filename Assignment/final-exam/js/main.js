let accountList = [];
let apiBase = "localhost:7777//api/v1/account"
// hàm mở đầu khi load trang sẽ chạy các function ở đây
let method = "";
let url = "";
$(function () {
  console.log();
  $("#header").load("./html/header.html");
  $("#body").load("./html/account.html");
  $("#footer").load("./html/footer.html");
  getListAccount();
});

function getListAccount() {
  // call api -> lấy ra danh sách account
  // gán gía trị vừa lấy được cho accountList
  $.ajax({
    url:  apiBase + "/get-all",
    type: "GET",
    contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
    // data: JSON.stringify(request),
    error: function (err) {
      // Hành động khi apii bị lỗi
      console.log(err);
      alert(err.responseJSON);
    },
    success: function (data) {
      // Hành động khi thành công
      fillDataToTable(data);
    },
  });
}

function fillDataToTable(data) {
  accountList = data;
  $("tbody").empty();
  accountList.forEach(function (item, index) {
    $("tbody").append(
      "<tr>" +
        '<td class="align-middle">' +
        (index + 1) +
        "</td>" +
        '<td><img id="image" src="' +
        item.avatar +
        '"></td>' +
        "<td>" +
        item.name +
        "</td>" +
        "<td>" +
        item.address +
        "</td>" +
        "<td>" +
        item.createdAt +
        "</td>" +
        "<td>" +
        '<a class="edit" title="Edit" data-toggle="tooltip" onclick="updateAccount(' +
        item.id +
        ')"><i class="fa fa-pencil"></i></a>' +
        '<a class="delete" title="delete" data-toggle="tooltip" onclick="confirmDelete(' +
        item.id +
        ')"><i class="fa fa-trash" aria-hidden="true"></i></a>' +
        "</td>" +
        "</tr>"
    );
  });
}

function clickNavihome() {
  $("#body").load("./html/home.html");
}

function clickNaviViewListAccount() {
  $("#body").load("./html/account.html");
  getListAccount();
}

function clickNaviViewListDepartment() {
  $("#body").load("./html/department.html");
}

function addnewAccount() {
  method = "POST";
  url = "https://643d4c7cf0ec48ce90586e00.mockapi.io/Account";
  resetForm();
}

function saveAccount() {
  // Lấy các giá trị có trong form -> tạo ra object -> call API
  let id = $("accountIdupdate").val();
  let username = document.getElementById("username").value;
  //   let username =$("username").val();
  console.log(username);
  let avatar = document.getElementById("avatar").value;
  //   let avatar =$("avatar").val();
  let password = document.getElementById("password").value;
  //   let password =$("password").val();
  let adress = document.getElementById("adress").value;
  //   let adress =$("adress").val();
  let createdAt = new Date();

  // Tạo object:
  const account = {
    name: username,
    avatar: avatar,
    password: password,
    address: adress,
    createdAt: createdAt,
  };
  console.log(method);

  // Call API
  $.ajax({
    url: url,
    type: method,
    contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
    data: JSON.stringify(account),
    error: function (err) {
      // Hành động khi api bị lỗi
      console.log(err);
      alert(err.responseJSON);
    },
    success: function (data) {
      // Hành động khi thành công
      getListAccount();
      $("#exampleModalCenter").modal("hide");
      showAlrtSuccess();
    },
  });
}

function showAlrtSuccess() {
  $("#success-alert")
    .fadeTo(2000, 500)
    .slideUp(500, function () {
      $("#success-alert").slideUp(3000);
    });
}

function deleteSuccess() {
  id = $("#accountIdDelete").val();
  $.ajax({
    url: "https://643d4c7cf0ec48ce90586e00.mockapi.io/Account/" + id,
    type: "DELETE",
    contentType: "application/json", // Định nghĩa định dạng dữ liệu truyền vào là json
    error: function (err) {
      // Hành động khi api bị lỗi
      console.log(err);
      alert(err.responseJSON);
    },
    success: function (data) {
      // Hành động khi thành công
      getListAccount();
      $("#modalConfirmDelete").modal("hide");
      showAlrtSuccess();
      $("#delete").modal("show");
    },
  });
}

$(document).ready(function () {
  document.getElementById("delete").addEventListener("click", deleteSuccess());
});

function resetForm() {
  // cách 1 : sử dụng Js thuần
  // documentdocd.getElementById("username").value = "";

  // cách 2 sử dụng Jquerry
  $("#username").val("");
  $("#avatar").val("");
  $("#adress").val("");
  $("#password").val("");
}

function confirmDelete(id) {
  $("#accountIdDelete").val(id);
  $("#modalConfirmDelete").modal("show");
}

function updateAccount(id) {
  const account = accountList.find((item, index) => {
    //  so sánh giá trị
    if (item.id == id) {
      return item;
    }
  });
  method = "PUT";
  url = "https://643d4c7cf0ec48ce90586e00.mockapi.io/Account/" +id;
  // Fill Thông tin ra ngoài form create/update
  $("#accountIdUpdate").val(account.id);
  $("#username").val(account.name);
  $("#avatar").val(account.avatar);
  $("#password").val(account.password);
  $("#address").val(account.address);
  $("#exampleModalCenter").modal("show");
}

function showId(id) {
  console.log(id);
}
