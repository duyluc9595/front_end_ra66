console.log(1234)
// Data type khai báo biến dùng let hoặc var (khuyên dùng let)
//Number : so
let Number = 10;
let Number2 = 10.1;

//String
let color = "red";

// boolean
let boolean2 = true;

//Object
let boolean = {id : "1", name :"Nguyễn Văn A"}
console.log(typeof{boolean2});

let Person1 = {id : "1", name : "Nguyễn V "}
let Person2 = {id : "2", name : "Nguyễn a "}
let Person3 = {id : "3", name : "Nguyễn b "}

//Array
let car = ["Honda", "Vin"]
console.log(car);
car.push("BMV") // Thêm phần tử
console.log(typeof{boolean2});

car.forEach(function(item){ //Lấy từng phần tử trong mảng console.log(item)
    console.log(item)
})

//Array Object
let Person = [Person1,Person2,Person3];
console.log(Person)
 const kq = Person.find((item, index) => {
    if(item.id === "2"){
        return item;
    }
})
console.log(kq);


// vào điều kiện đúng khi biến truyền vào có giá trị , và khác false, 0,  null, undefined
if(color){
    console.log("vào đây nếu điều kiện đúng")
}else{
    console.log("điều kiện sai")
}

name_function();

function name_function(){
  console.log("Tên Function");
}

function Account(id, name, age){
    this.id = id;
    this.Account = name;
    this.age = age;
}

let account1 = new Account(1, "account 1", 10);
console.log(account1);