package com.vti.storyproject.Repository;

import com.vti.storyproject.modal.entity.ReviewsNovels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewNovelsRepository extends JpaRepository<ReviewsNovels, Integer> {
}
