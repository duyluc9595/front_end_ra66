function CreatReview(content, rating, accountId, novelsIds) {
    this.content = content;
    this.rating = rating;
    this.accountId = accountId;
    this.novelsIds = novelsIds;

  }


let baseUrlReviewNovels = "http://localhost:8686/api/v1/review";

function addReview(){
    let novelsReviewId = $("#Novels-Review-Id").val()
    let reviewContent = $('#review-content').val()
    let stars = 0;
    let accountId = localStorage.getItem("id")
    for (let i = 1; i < 6; i++){
       let star = $('#star-'+ i).val();
       if (star != ''){
        stars = $('#star-'+ i).val();
        $('#star-'+ i).val('')
       }
    }

    let request = new CreatReview(reviewContent, stars, accountId, novelsReviewId);

    $.ajax({
        url: baseUrlReviewNovels + "/create",
        type: "POST",
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem("token"));
        },
        contentType: "application/json",
        data: JSON.stringify(request),
        error: function (err) {
          confirm(err.responseJSON.message)
        }
      });
      
    }